const getSum = (str1, str2) => {
  let result;
  if (typeof str1 !== 'string' || typeof str2 !== 'string') {
    result = false;
  } else if (parseInt(str1, 10) && parseInt(str2, 10)) {
    result = (parseInt(str1, 10) + parseInt(str2, 10)).toString();
  } else if (!parseInt(str1, 10) || !parseInt(str2, 10)) {
    if (parseInt(str1, 10)) {
      result = str1;
    } else if (parseInt(str2, 10)) {
      result = str2;
    } else {
      result = false;
    }
  }
  return result;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comments = 0;
  for (let i = 0; i < listOfPosts.length; i++) {
    if (listOfPosts[i].author === authorName) {
      posts++;
    }
    for (const comment in listOfPosts[i].comments) {
      if (listOfPosts[i].comments[comment].author === authorName) {
        comments++;
      }
    }
  }
  return `Post:${posts},comments:${comments}`;
};

const tickets = (people) => {
  let money = 0;
  for (let i = 0; i < people.length; i++) {
    if (people[i] === 25) {
      money += people[i];
    } else if (money === 0) {
      return 'NO';
    } else {
      money -= 25;
    }
  }
  return 'YES';
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
